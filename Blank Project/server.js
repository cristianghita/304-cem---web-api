var express = require('express');
var swig = require('swig');

var app = express();
app.use(express.static('public'));

var port = 8080;

app.get('/', function(req, res) {
	var template = swig.compileFile(__dirname + '/public/html/index.html') // change index.html
	
	var output = template({});
	
	res.send(output);
})

app.listen(port, err => {
	if (err) {
		cosnole.error(err)
	} else {
		console.log(`App is ready on port ${port}`)
	}
});