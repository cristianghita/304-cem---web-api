var buttonPrint = document.getElementById("buttonPrint");

buttonPrint.onclick = function() {
	window.print();
}

var buttonModal = document.getElementById("buttonModal");
var modal = document.getElementById("myModal");
var span = document.getElementsByClassName("close")[0];

buttonModal.onclick = function() {
	modal.style.display = "block";
}

span.onclick = function() {
	modal.style.display = "none";
}

window.onclick = function(event) {
	if (event.target == modal) {
		modal.style.display = "none";
	}
}