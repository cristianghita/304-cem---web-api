//import express package and savei t in the express variable
var express = require('express');
var swig = require('swig')

//create a new instance of express and save it in a variable called app
var app = express();
app.use(express.static('public'));

//save port globally
var port = 8080;

app.get('/', function(req, res){
	//read the html page
	var template = swig.compileFile(__dirname + '/public/html/cv.html')

	//apply any template variables, currently we have none
	var output = template({});

	res.send(output);
});

//now run the server at port 8080
app.listen(port); 