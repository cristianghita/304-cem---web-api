console.log("Hello World!")

alterMessage();

function alterMessage() {
	var h1Message = document.getElementById("helloMessage");
	h1Message.innerHTML = multiply(5, 6);
	//h1Message.innerHTML = "I altered your message";
	//h1Message.style.fontSize = "40px";
	//h1Message.style.display = 'none';
}

function multiply(a, b){
	return a * b;
}

function changeText(id){
	id.innerHTML = "Muie PSD";
}

function changeBackground(id){
	id.style.backgroundColor = id.style.backgroundColor == "blue" ? "white" : "blue";
}

var modal = document.getElementById('myModal');
var btn = document.getElementById('myButton');
var span = document.getElementsByClassName("close")[0];

btn.onclick = function() {
	modal.style.display = "block";
}

span.onclick = function() {
	modal.style.display = "none";
}

window.onclick = function(event) {
	if (event.target == modal){
		modal.style.display = "none";
	}
}
