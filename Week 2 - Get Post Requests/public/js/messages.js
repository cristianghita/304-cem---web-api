var db = require('./database');

exports.add = function(conData, req, callback) {
    
    db.connect(conData, function(err, data) {
        
        if(err) {
            callback(err);
            return;
        }

        var message = {
            from: req.body['fromName'],
            email: req.body['fromEmail'],
            url: req.body['url'],
            message: req.body['message']
        };

        data.query('INSERT INTO messages SET ?', message, function (err, result) {
            callback(err, message);
        });
    });
};

exports.getById = function(conData, req, callback) {
    
    //connect  to db
    db.connect(conData, function(err, data) {
        //conn done => check if error
        if (err) {
            callback(err);
            return;
        }

        let id = req.params.id;

        data.query('SELECT * FROM messages WHERE id = ' + id, function(err, result) {
            let data = JSON.stringify(result);
            callback(err, data);
        })
    })
}

exports.getAll = function(conData, req, callback) {
    db.connect(conData, function(err, data) {
        if (err) {
            callback(err);
            return;
        }

        data.query('SELECT * FROM messages', function(err, result) {
            let data = JSON.stringify(result);
            callback(err, data);
        })
    })
}

exports.deleteById = function(conData, req, callback) {
    db.connect(conData, function(err, data){ 
        if (err) {
            callback(err);
            return;
        }

        let id = req.params.id;

        data.query('DELETE FROM messages WHERE id = ' + id, function(err, result) {
            callback(err, "success");
        })
    })
}

exports.updateById = function(conData, req, callback) {
    db.connect(conData, function(err, data) {
        if(err) {
            callback(err);
            return;
        }

        let id = req.params.id;
        let from = req.params.from;
        let email = req.params.email;
        let url = req.params.url;

        data.query('UPDATE messages SET from = ' + from + '; email = ' + email + '; url = ' + url + ' WHERE id = ' + id,
            function(err, result) {
                callback(err, "success");
            })
    })
}