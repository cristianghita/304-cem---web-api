var express = require('express');
var swig = require('swig');

var messages = require('./public/js/messages');
var db = require('./public/js/database');

var app = express();
app.use(express.static('public'));

var bodyParser = require('body-parser');
app.use(bodyParser.urlencoded({extended: false}));

var port = 8080;

const databaseData = {
	host: 'sql2.freemysqlhosting.net',
	user: 'sql2261595',
	password: 'pL9!pX7*',
	database: 'sql2261595'
};

app.get('/', function(req, res) {
	var template = swig.compileFile(__dirname + '/public/html/index.html') // change index.html
	
	var output = template({});
	
	res.send(output);
})

app.get('/process_contact_submission', function(req, res) {
	var template = swig.compileFile(__dirname + '/public/html/index.html') // change index.html
	
	var output = template({});
	
	res.send(output);
})

app.post('/process_contact_submission', function(req, res) {
	
	var data = {
		fromName: req.body.fromName,
		fromEmail: req.body.fromEmail
	};

	/*
	console.log(res)
	res.end(JSON.stringify(data));
	*/

	messages.add(databaseData, req, function(err, data) {
			
		if (err) {
			res.status(400);
			res.end("error: " + err);
		}

		res.status(201);
		res.end("success");
	})
})

app.get('/createTables', (req, res) => {
	db.createTables(databaseData, function(err, state) {
		if (err) {
			res.status(400);
			res.end("an error has occured:" + err);
			return;
		}

		res.status(200);
		res.end("tables were created successfully");
	})
})

app.post('/api/v1.0/messages', (req, res) => {
	messages.add(databaseData, req, function(err, data) {
		//after adding a new message, this code will run

		//tell the client we sending json data
		res.setHeader('content-type', 'application/json')
		res.setHeader('accepts', 'GET, POST')

		if (err){
			res.status(400);
			res.end("error: " + err);
			return;
		}

		res.status(201);
		res.end(JSON.stringify({message: "user added successfully"}));
	})
})

app.get('/api/v1.0/messages', (req, res) => {
	message.getAll(databaseData, req, function(err, data) {
		res.setHeader('content-type', 'application/json')
		res.setHeader('accepts', 'GET')

		if (err){
			res.status(400);
			res.end("error: " + err);
			return;
		}

		res.status(200);
		res.end(data);
	})
})

app.get('/api/v1.0/messages/:id', (req, res) => {
	
	messages.getById(databaseData, req, function(err, data) {
		res.setHeader('content-type', 'application/json')
		res.setHeader('accepts', 'GET')

		if (err){
			res.status(400);
			res.end("error: " + err);
			return;
		}

		res.status(200);
		res.end(data);
	})
})

app.put('/api/v1.0/messages/:id/:from/:email/:url/:message/', (req, res) => {
	messages.updateById(databaseData, req, function(err, data) {
		if (err){
			res.status(400);
			res.end("error: " + err);
			return;
		}

		res.status(200);
		res.end("success");
	})
})

app.delete('/api/v1.0/messages/:id', (req, res) => {
	messages.deleteById(databaseData, req, function (err, data) {
		if (err){
			res.status(400);
			res.end("error: " + err)
			return;
		}

		res.status(201);
		res.end(data);
	})
})

app.listen(port, err => {
	if (err) {
		cosnole.error(err)
	} else {
		console.log(`App is ready on port ${port}`)
	}
});

